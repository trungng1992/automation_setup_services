#!/bin/bash

#Author: jok3r


wget --no-check-certificate -O /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 http://mirror.centos.org/centos/7/os/x86_64/RPM-GPG-KEY-CentOS-7
wget --no-check-certificate -O /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 https://getfedora.org/static/352C64E5.txt


# Setup neccessary packages/settings
sed -i 's/\(.*enabled=.*\)/enabled=0/g' /etc/yum/pluginconf.d/fastestmirror.conf
sed -i "s/SELINUX=enforcing/SELINUX=disabled/" /etc/selinux/config

rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

# Setup epel
yum -y install epel-release
yum -y install libstdc++*
yum -y install glib* gnutls-devel readline-devel python-devel libaio-devel freetype-devel freetype zlib-devel
yum -y groupinstall "Compatibility libraries" "Development Tools" "Base"
yum -y install git tree python-devel dmidecode openipmi openipmi-tools pciutils lshw* ipmitool man mlocate vim nano screen ntpdate gcc-c++ autoconf automake rsync tcpdump wget unzip openssl net-snmp net-snmp-utils iptables-services openssl-devel net-tools libstdc* libsysfs sysfsutils telnet ipmi* gdisk parted xfsprogs xfsdump fdisk resize2fs nmap bind-utils tcptraceroute arp-scan lynx yum-utils iotop iostat
yum -y install ncurses-devel bison flex openssl-devel

#Upgrade openssh
cd /opt
wget --no-check-certificate -O /opt/openssh-7.6p1-rpm-centos7.tar.gz https://baremetal.vng.com.vn/public/packages/linux/openssh/openssh-7.6p1-rpm-centos7.tar.gz
tar xvf openssh-7.6p1-rpm-centos7.tar.gz
cp /etc/pam.d/sshd /tmp/pam.sshd.backup
cp /etc/ssh/sshd_config /tmp/config.sshd.backup
rpm -Uvh openssh-*.rpm
rm -f  /etc/ssh/sshd_config
rm -f /etc/pam.d/sshd
mv /tmp/config.sshd.backup /etc/ssh/sshd_config
mv /tmp/pam.sshd.backup /etc/pam.d/sshd


# Remove unsupported entry config sshd in new version
sed -i '/ShowPatchLevel/d' /etc/ssh/sshd_config
sed -i '/UsePrivilegeSeparation/d' /etc/ssh/sshd_config
sed -i '/UseLogin/d' /etc/ssh/sshd_config
sed -i '/RSAAuthentication/d' /etc/ssh/sshd_config
sed -i '/RhostsRSAAuthentication/d' /etc/ssh/sshd_config

# re-generate host key
rm -f /etc/ssh/ssh_*_key
systemctl restart sshd
service sshd restart
chmod 400 /etc/ssh/ssh_*_key
sshd -t

yum install open-vm-tools -y
/etc/init.d/vmtoolsd start
chkconfig vmtoolsd on