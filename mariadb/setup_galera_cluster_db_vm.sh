#!/bin/bash

#Author: Jok3r

IP_PRIVATE=$1
IP_SECOND=$2
IP_THIRD=$3
NAME_NODE=$4
cat <<EOF > /etc/yum.repos.d/MariaDB.repo
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.4/rhel7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF

yum install MariaDB-server MariaDB-client -y
sudo systemctl start mysql.service


mysql_secure_installation <<EOF
y
y
database
somepass
y
y
y
y
y
EOF

cat <<EOF > /etc/my.cnf.d/server.cnf
[server]
datadir=/vserver/mysql
[mysqld]
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
bind-address=${IP_PRIVATE}
innodb_buffer_pool_size=6G
query_cache_size=0
innodb_log_file_size=2G
tmp-table-size=1G
max_allowed_packet=11
transaction-isolation = READ-COMMITTED
max-heap-table-size=1G
key_buffer=2G
thread_concurrency=16
thread-handling=pool-of-threads
open_files_limit = 50000
[galera]
wsrep_on=ON
wsrep_provider=/usr/lib64/galera-4/libgalera_smm.so
wsrep_sst_method=rsync
wsrep_node_address="${IP_PRIVATE}"
wsrep_node_name="${NAME_NODE}"
wsrep_retry_autocommit=10
wsrep_cluster_address='gcomm://${IP_PRIVATE},${IP_SECOND},${IP_THIRD}'
wsrep_cluster_name='vclouddrive'

[embedded]
[mariadb]
[mariadb-10.4]
EOF