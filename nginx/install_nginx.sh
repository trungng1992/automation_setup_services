#!/bin/bash

#####################
#Build source nginx
#####################
. ./common.sh
VERSION_NGINX=`read_property VERSION_NGINX`
VERSION_PCRE=`read_property VERSION_PCRE`
VERSION_ZLIB=`read_property VERSION_ZLIB`
VERSION_OPENSSL=`read_property VERSION_OPENSSL`

VERSION_LUAJIT=`read_property LUAJIT_VERSION`
VERSION_LUAJIT_MAJOR=`read_property LUAJIT_MAJOR_VERSION`
VERSION_NGINX_DEV_KIT=`read_property NGX_DEVEL_KIT_VERSION`
VERSION_LUA_NGINX_MODULE=`read_property LUA_NGINX_MODULE_VERSION`

VERSION_NGINX_VTS=`read_property VERSION_NGINX_VTS`
VERSION_NGINX_UPSTREAM_CHECK_MODULES=`read_property VERSION_UPSTREAM_CHECK_MODULE`

NUM_JOB=`read_property NUM_JOB`

#yum install necessary packages
yum groupinstall -y "Development Tools"
yum install -y gcc-c++ perl pcre-devel zlib-devel epel-release perl-ExtUtils-Embed pam-devel -y

cd /tmp

if [ ! -f ./nginx-${VERSION_NGINX}.tar.gz ]; then
    wget http://nginx.org/download/nginx-${VERSION_NGINX}.tar.gz
fi

if [ ! -f ./zlib-${VERSION_ZLIB}.tar.xz ]; then
    wget https://www.zlib.net/zlib-${VERSION_ZLIB}.tar.xz
fi

if [ ! -f ./pcre-${VERSION_PCRE}.tar.gz ]; then
    wget https://ftp.pcre.org/pub/pcre/pcre-${VERSION_PCRE}.tar.gz
fi

if [ ! -f ./openssl-${VERSION_OPENSSL}.tar.gz ]; then
    wget https://www.openssl.org/source/openssl-${VERSION_OPENSSL}.tar.gz
fi

if [ ! -f ./LuaJIT-${VERSION_LUAJIT}.tar.gz ]; then
    wget http://luajit.org/download/LuaJIT-${VERSION_LUAJIT}.tar.gz
fi

if [ ! -f ./ngx_dev_kit_${VERSION_NGINX_DEV_KIT}.tar.gz ]; then
    wget https://github.com/simpl/ngx_devel_kit/archive/v${VERSION_NGINX_DEV_KIT}.tar.gz \
        -O ngx_dev_kit_${VERSION_NGINX_DEV_KIT}.tar.gz
fi

if [ ! -f ./lua-nginx-module-${VERSION_LUA_NGINX_MODULE}.tar.gz ]; then
    wget https://github.com/openresty/lua-nginx-module/archive/v${VERSION_LUA_NGINX_MODULE}.tar.gz \
        -O lua-nginx-module-${VERSION_LUA_NGINX_MODULE}.tar.gz
fi

if [ ! -f ./v${VERSION_NGINX_VTS}.tar.gz ]; then
    wget https://github.com/vozlt/nginx-module-vts/archive/v${VERSION_NGINX_VTS}.tar.gz \
        -O nginx-module-vts-${VERSION_NGINX_VTS}.tar.gz
fi

if [ ! -f ./v${VERSION_NGINX_UPSTREAM_CHECK_MODULES}.tar.gz ]; then
    wget https://github.com/yaoweibin/nginx_upstream_check_module/archive/v${VERSION_NGINX_UPSTREAM_CHECK_MODULES}.tar.gz \
        -O nginx-nginx_upstream_check_module-${VERSION_NGINX_UPSTREAM_CHECK_MODULES}.tar.gz
fi

ls *.tar.gz | xargs -i tar xvf {}
ls *.tar.xz | xargs -i tar xvf {}

#build luajt
# cd /tmp/LuaJIT-${VERSION_LUAJIT}
# make -j ${NUM_JOB}
# make install -j ${NUM_JOB}

#back forward directory
cd /tmp

#create user nginx
useradd nginx

#make directory
mkdir -p /var/webapp
mkdir -p /var/run/nginx
mkdir -p /etc/nginx
mkdir -p /var/cache/nginx/

NGX_DEVEL_KIT_PATH=$(pwd)/ngx_devel_kit-${VERSION_NGINX_DEV_KIT}
LUA_NGINX_MODULE_PATH=$(pwd)/lua-nginx-module-${VERSION_LUA_NGINX_MODULE}
NGX_MODULE_VTS_PATH=$(pwd)/nginx-module-vts-${VERSION_NGINX_VTS}
NGX_UPSTREAM_CHECK_MODULE_PATH=$(pwd)/nginx_upstream_check_module-${VERSION_NGINX_UPSTREAM_CHECK_MODULES}

# LUAJIT_LIB=/usr/local/lib/lua
# LUAJIT_INC=/usr/local/include/luajit-${VERSION_LUAJIT_MAJOR}

cd /tmp/nginx-*

patch -p1 < ${NGX_UPSTREAM_CHECK_MODULE_PATH}/check_1.14.0+.patch

./configure \
--prefix=/var/webapp \
--sbin-path=/usr/sbin/nginx \
--conf-path=/etc/nginx/nginx.conf \
--pid-path=/var/run/nginx/nginx.pid \
--with-pcre=../pcre-$VERSION_PCRE \
--with-zlib=../zlib-$VERSION_ZLIB \
--with-http_ssl_module \
--with-stream \
--with-http_v2_module \
--with-threads \
--without-mail_pop3_module \
--without-mail_imap_module \
--without-mail_smtp_module \
--user=nginx \
--group=nginx \
--error-log-path=/var/log/nginx/error.log \
--lock-path=/var/run/nginx.lock \
--build=CentOS \
--modules-path=/usr/lib64/nginx/modules \
--with-http_gunzip_module \
--with-http_gzip_static_module \
--http-log-path=/var/log/nginx/access.log \
--http-client-body-temp-path=/var/cache/nginx/client_temp \
--http-proxy-temp-path=/var/cache/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
--http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
--http-scgi-temp-path=/var/cache/nginx/scgi_temp \
--with-stream_ssl_module \
--with-compat \
--with-http_sub_module \
--with-http_realip_module \
--with-poll_module \
--with-http_geoip_module \
--with-openssl=../openssl-$VERSION_OPENSSL \
--with-cc-opt=-O2 \
--with-ld-opt='-Wl,-rpath,/usr/local/lib' \
--add-module=${NGX_MODULE_VTS_PATH} \
--add-module=${NGX_UPSTREAM_CHECK_MODULE_PATH}
# --add-module=${NGX_DEVEL_KIT_PATH} \
# --add-module=${LUA_NGINX_MODULE_PATH} \

make -j $NUM_JOB
make install -j $NUM_JOB

#Copy config
yes | cp -f etc/*.conf /etc/nginx/*

#Generate ssl self-signed
HOST="localhost"
CN="*.vinadata.vn"
COUNTRY="VN"
STATE="Ho Chi Minh"
LOCALITY="Ho Chi Minh"
ORGANIZATION="jok3r"
ORGANIZATIONALUNIT="jok3r"
EMAIL="trung.ng1992@gmail.com"

if [ ! -f /opt/ssl/self-signed.crt ]; then
  mkdir /opt/ssl
  openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /opt/ssl/self-signed.key -out /opt/ssl/self-signed.crt \
      -subj "/C=${COUNTRY}/ST=${STATE}/L=${LOCALITY}/O=${ORGANIZATION}/OU=${ORGANIZATIONALUNIT}/CN=${CN}/emailAddress=${EMAIL}"
  openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
fi
