#!/bin/bash

read_property() (
  # The property we are looking for.
  prop_name=$1

  # The value of the property set initially to empty string.
  prop_value=

  if [ ! "$prop_name" = "" ] ; then
    # Search in the main '.config' file.
    prop_value="`grep -i ^${prop_name}= .config | cut -f2- -d'=' | xargs`"
  fi
  echo "$prop_value"
)

restart_httpd() (
  isSystemd=$1
  if [ $isSystemd == 1 ]; then
    systemctl restart httpd
  else
    services httpd restart
  fi
)

restart_nginx() (
  nginx -s reload
)

test_nginx() (
  nginx -t    
)