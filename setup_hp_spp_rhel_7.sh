#!/bin/bash

#Author: Jok3r


cat <<< '
[hp-spp]
name=Extra Packages for Enterprise Linux 7 - $basearch
baseurl=https://downloads.linux.hpe.com/SDR/repo/spp/RHEL/7/x86_64/current/
#metalink=https://mirrors.fedoraproject.org/metalink?repo=epel-7&arch=$basearch
failovermethod=priority
enabled=1
gpgcheck=0
' > /etc/yum.repos.d/hp_spp.repo

if [[ $(dmidecode -t1 | grep Product) =~ "Gen10" ]]; then
    yum install -y net-snmp net-snmp-utils amsd
    
    systemctl disable amsd && systemctl stop amsd
    systemctl disable amsd_rev && systemctl stop amsd_rev
    systemctl disable smad && systemctl stop smad
    systemctl disable smad_rev && systemctl start smad_rev
else
    yum install -y net-snmp net-snmp-utils hp-health hp-snmp-agents  ssacli
    
    systemctl restart hp-health
    systemctl restart hp-snmp-agents
fi