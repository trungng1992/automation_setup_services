#!/bin/bash

echo "Install stastd base on docker"

echo "Checking docker is installed?"

if [[ ! -f /bin/docker ]]; then
    echo "Docker is not installed."
    echo "Please install docker beforce run this scripts"
    exit 1
else
    echo "Docker is installed. Done"
fi

echo "Checking services docker"

if [[ -f /bin/systemctl ]]; then
    systemctl status docker 2>&1 | grep "Active" | grep "inactive"
    if [ $? -eq 0 ]; then
        echo "Docker is stoped"
        systemctl start docker
    else
        echo "Docker is running"
    fi
else
    echo "By-pass checking docker services"
fi

