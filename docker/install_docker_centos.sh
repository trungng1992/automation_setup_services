#!/bin/bash

# Author: JokEr

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi


usage(){
    echo "#This script use to setup docker-ce on CentOS.\n"
    echo "#It will be removed installed docker and installed new version.\n"
    echo "#BE CAREFUL: to not run this script if you not have read this scripts yets."
    echo "#Please contact me if you wanna get more information."
}

rpm -qa | grep docker
if [ $? -ne 0 ]; then
    echo "#Remove Docker....\n"
    yum remove -y docker*
fi

#yum necessary
yum install -y yum-utils device-mapper-persistent-data

#add repository
wget -O /opt/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo
if [ ! -f /opt/docker-ce.repo ]; then
    echo "Can not download docker-ce.repo \n"
    echo "Please check proxy or dns. \n"
    exit 1
fi

yum-config-manager --add-repo /opt/docker-ce.repo
yum-config-manager --enable docker-ce

echo "Install docker-ce and docker-compose"
#echo "y" to agree with gpg keys
echo "y" | echo "y" | yum install -y docker-ce docker-ce-cli containerd.io


echo "Install docker-compose"
os=$(cat /etc/redhat-release)
if [[ $os =~ " 6." ]]; then
    rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6
elif [[ $os =~ " 7." ]]; then
    rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-7
elif [[ $os =~ " 8." ]]; then
    rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-8
else
    rpm --import https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL
fi

#Edit
yum install -y epel-release
yum install -y docker-compose

if [ $? -ne 0 ]; then
    echo "Can not install docker-ce. \n"
    exit 1
fi

systemctl start docker