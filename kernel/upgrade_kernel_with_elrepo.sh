#!/bin/bash

# Author: JokEr

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

os_release=$(cat /etc/redhat-release)

if [[ $os_release =~ " 6." ]]; then
    rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
    yum install -y https://www.elrepo.org/elrepo-release-6-9.el6.elrepo.noarch.rpm
elif [[ $os_release =~ " 7." ]]; then
    rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
    yum install -y https://www.elrepo.org/elrepo-release-7.0-4.el7.elrepo.noarch.rpm
elif [[ $os_release =~ " 8." ]]; then
    rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
    yum install -y https://www.elrepo.org/elrepo-release-8.0-2.el8.elrepo.noarch.rpm
else
    echo "OS is not support"
    exit 1
fi

yum --enablerepo=elrepo-kernel -y install kernel-lt

#Get ordered list menu boot
awk -F\' '$1=="menuentry " {print i++ " : " $2}' /etc/grub2.cfg
#Example output:
#0 : CentOS Linux (4.4.186-1.el7.elrepo.x86_64) 7 (Core)
#1 : CentOS Linux (3.10.0-957.21.3.el7.x86_64) 7 (Core)
#2 : CentOS Linux (3.10.0-862.el7.x86_64) 7 (Core)
#3 : CentOS Linux (0-rescue-3fc8b0b40175479e976fcb96bd058ba6) 7 (Core)


#Chose default boot-grub
#grub2-set-default <id>
grub2-set-default 0

#Re-generate grub2.cfg
grub2-mkconfig -o /boot/grub2/grub.cfg

yum install -y kernel-lt-devel kernel-lt-tools-libs kernel-lt-headers
#Reboot and take it
reboot

#End_scripts.   